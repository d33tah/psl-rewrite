extern crate psllib;
use psllib::{return_root, Node};

use std::io::{self, BufRead};

fn _lookup_node(hits: &mut Vec<bool>, depth: usize, children: &Node<&str>, parts: &Vec<&str>) {
    let size = hits.len();
    hits[size - depth] = true;
    if depth >= size || children.children.is_empty() {
        return;
    }
    for ref child in children.children {
        // println!("child={:?}, el={:?}", child.name, parts[size - depth]);
        if child.name == "*" {
            _lookup_node(hits, depth + 1, child, parts);
        }
        if child.name == parts[size - depth] {
            _lookup_node(hits, depth + 1, child, parts);
        }

    }
}

fn get_public_suffix(start: &Node<&str>, domain: &str) -> String {
    let parts: Vec<&str> = domain.split(".").collect();
    let mut hits: Vec<bool> = vec![false; parts.len()];
    _lookup_node(&mut hits, 1, start, &parts);
    // println!("parts={:?}, hits={:?}", parts, hits);
    for (i, what) in hits.iter().enumerate() {
        if *what {
            return parts[i..parts.len()].join(".");
        }
    }
    return "".into();
}


fn main() {
    let root2 = return_root();
    let stdin = io::stdin();
    for line_res in stdin.lock().lines() {
        let line = line_res.unwrap();
        let line2 = line.trim_right();
        let s: &str = &*line2;
        println!("{}", get_public_suffix(&root2, s));
    }
}
