import publicsuffix
psl = publicsuffix.PublicSuffixList()

import sys

root = {'uk': {'ac': {},
    'co': {'blogspot': {}},
    'gov': {'service': {}},
    'ltd': {},
    'me': {},
    'net': {},
    'nhs': {},
    'org': {},
    'plc': {},
    'police': {},
    'sch': {'*': {}}}}


def visit(x, n=0):
    if x == 0 or x == 1:
        d = {}
    else:
        d = x[1]
    first = True
    for k in d:

        sys.stdout.write('\t' * n)
        if first:
            first = False
        else:
            sys.stdout.write(', ')

        sys.stdout.write('Node { name: "%s", children: &[\n' % k)
        visit(d[k], n+1)
        sys.stdout.write('\t' * n)
        sys.stdout.write(']}')
        sys.stdout.write('\n')


def to_rust(d, n):
    first = True
    for k in d:
        sys.stdout.write('\t' * n)
        if first:
            first = False
        else:
            sys.stdout.write(', ')

        sys.stdout.write('Node { name: "%s", children: &[\n' % k)
        to_rust(d[k], n+1)
        sys.stdout.write('\t' * n)
        sys.stdout.write(']}')
        sys.stdout.write('\n')

print('''
#[derive(Debug)]
pub struct Node<T: 'static> {
    pub name: T,
    pub children: &'static [Node<T>],
}''')

sys.stdout.write("pub fn return_root() -> Node<&'static str> { return Node { "
        'name: "root", children: &[ ')
visit(psl.root)
#visit([0, {'uk': psl.root[1]['uk']}])
#to_rust(root, 0)
sys.stdout.write(']}; }')
