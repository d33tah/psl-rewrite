root = {'uk': {'ac': {},
    'co': {'blogspot': {}},
    'gov': {'service': {}},
    'ltd': {},
    'me': {},
    'net': {},
    'nhs': {},
    'org': {},
    'plc': {},
    'police': {},
    'sch': {'*': {}}}}

import sys
def to_rust(d, n):
    first = True
    for k in d:
        sys.stdout.write('\t' * n)
        if first:
            first = False
        else:
            sys.stdout.write(', ')

        sys.stdout.write('Node { name: "%s", children: vec![\n' % k)
        to_rust(d[k], n+1)
        sys.stdout.write('\t' * n)
        sys.stdout.write(']}')
        sys.stdout.write('\n')

to_rust({'root': root}, 0)


def _lookup_node(matches, depth, children, parts):
    matches[-depth] = True
    if depth >= len(parts) or not children:
        return
    if parts[-depth] in children:
        _lookup_node(matches, depth+1, children[parts[-depth]], parts)
    if '*' in children:
        _lookup_node(matches, depth+1, children['*'], parts)

def get_public_suffix(root, domain):
    parts = domain.lower().strip('.').split('.')
    hits = [False] * len(parts)
    _lookup_node(hits, 1, root, parts)
    print(parts)
    print(hits)
    for i, what in enumerate(hits):
        if what:
            return '.'.join(parts[i:])

get_public_suffix(root, 'test.co.uk')

d = [
    'hai.uk',
    'test.blogspot.co.uk',
    'test2.blogspot2.co.uk',
    'test2.sch.uk',
    'test3.sch.uk',
    'test4.test5.sch.uk',
    'test6.test7.test1.sch.uk',
]

d2 = [get_public_suffix(root, x) for x in d]

d3 =['hai.uk', 'test.blogspot.co.uk', 'blogspot2.co.uk', 'test2.sch.uk',
     'test3.sch.uk', 'test4.test5.sch.uk', 'test7.test1.sch.uk']
if d2 != d3:
    print('Fail!')
    print(d2)
    print(d3)
